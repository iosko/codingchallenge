# README #

This is the SOAP client referred to by <https://bitbucket.org/swisscomhealth-devops/coding-challenge-backend/src/master/README.md>

## Building and Running ##

* Clone the repository: `git clone https://iosko@bitbucket.org/iosko/codingchallenge.git`
* Generate the jar file: `mvn clean package`
* Run the jar file: `java -jar target/CodingChallenge-0.0.1-SNAPSHOT.jar`
* Use the service, visit e.g. `http://localhost:8080/simplePatient/CH`

### Limitations & points to improve ###

* Currently the app has the WSDL included in the repository as well as the generated Java files  
That is due to the fact that I was not able to properly build the SOAP Java files from the WSDL using jaxb.
The current alternative is to to use wsimport and manually store the files in the repository.
But even this does not allow to run the SOAP client inside the Intellij IDE or with `mvn spring-boot:run`.
Only with `java -jar target/CodingChallenge-0.0.1-SNAPSHOT.jar` does it work. I deem it the same problem as this: <https://stackoverflow.com/questions/48292172/how-to-resolve-interface-is-not-visible-from-class-loader-in-java>  
It shoult be possible to build the application without the SOAP Java files stored in the repository.
