package net.ihe.gazelle.codingchallenge;

import net.ihe.gazelle.codingchallenge.soapclient.DemographicDataServer;
import net.ihe.gazelle.codingchallenge.soapclient.DemographicDataServerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class CodingChallengeApplication {
	public static void main(String[] args) {
		SpringApplication.run(CodingChallengeApplication.class, args);
	}

}
