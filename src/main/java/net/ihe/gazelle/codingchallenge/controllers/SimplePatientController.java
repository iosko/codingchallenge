package net.ihe.gazelle.codingchallenge.controllers;

import net.ihe.gazelle.codingchallenge.soapclient.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.invoke.MethodHandles;

@RestController
@RequestMapping(SimplePatientController.BASE_URL)
public class SimplePatientController {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final DemographicDataServer demographicDataServer = new DemographicDataServerService().getDemographicDataServerPort();

    static final String BASE_URL = "/";

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value="simplePatient/{country}")
    public ResponseEntity<Patient> getSimplePatient(@PathVariable String country) {
        logger.info("getSimplePatient, country:" + country);

        try {
            Patient patient = demographicDataServer.returnSimplePatient(country);
            return ResponseEntity.status(HttpStatus.OK).body(patient);
        } catch (SOAPException_Exception | JDOMException_Exception | IOException_Exception e) {
            logger.error("unknown error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

}
